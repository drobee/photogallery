<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'title', 'url', 'gallery_id',
    ];

    protected $table = 'photos';

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
}
