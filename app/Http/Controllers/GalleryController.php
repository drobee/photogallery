<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Gallery as Gallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255|regex:/^[\pL\s\d]+$/u',
        ]);

        if(!$request->has('title'))
            return response()->json(['errors' => ['status' => '400', 'title' => 'Rossz bemenet', 'detail' => 'Nem adtál meg címet' ]], 400);
        $user = Auth::user();
        $gallery = Gallery::create(['title' => $request->title, 'user_id' => $user->id, 'active' => 1, 'shareUrl' => substr(md5($request->title . time()),0,10)]);
        $result = File::makeDirectory(base_path() . '/public/photos/' . $gallery->shareUrl);
        if($result)
            return response()->json(["data" => [$gallery]]);
        else {
            $gallery->delete();
            return response()->json(['errors' => ['status' => '500', 'title' => 'Szerverhiba miatt nem sikerült létrehozni a galériát']], 500);            
        }
    }

    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        $user = Auth::user();
        $gallery = Gallery::where('id', $request->id)->where('user_id', $user->id)->first();
        if($gallery)
            $deletedRows = $gallery->delete();
        else
            return response()->json(['errors' => ['status' => '400', 'title' => 'A galéria nem törölhető']], 400); 
        if($deletedRows)
            return response()->json(["data" => ['status' => 'Gallery deleted']]);
        else
            return response()->json(['errors' => ['status' => '400', 'title' => 'A galéria nem törölhető']], 400); 
    }

    public function photos(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        $user = Auth::user();
        $gallery = Gallery::where('id', $request->id)->where('user_id', $user->id)->with(['photos'])->first();
        if(!$gallery)
            return response()->json(['errors' => ['status' => '401', 'title' => 'Nincs joga a galéria megtekintéséhez']], 401);
        return response()->json(['data' => $gallery]); 
    }

    public function publicPhotos($url)
    {
        $gallery = Gallery::where('shareUrl', $url)->with('photos')->first();
        return response()->json(['data' => $gallery]); 
    }
}
