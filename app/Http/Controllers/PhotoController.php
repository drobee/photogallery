<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Gallery as Gallery;
use App\Photo as Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PhotoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'max:255|regex:/^[\pL\s\d]+$/u',
            'image' => 'required|image|max:4096',
            'gallery' => 'required|numeric'
        ]);
        $title = $request->has('title') ? $request->title: "";
        $user = Auth::user();
        $gallery = Gallery::where('id',$request->gallery)->where('user_id',$user->id)->first();
        if(!$gallery)
            return response()->json(['errors' => ['status' => '400', 'title' => 'Hibás galéria azonosító']], 400);             

        $file = $request->file('image');
        $filename = iconv(mb_detect_encoding($file->getClientOriginalName(), mb_detect_order(), true), "UTF-8", $file->getClientOriginalName());            
        $extension = $file->getClientOriginalExtension();
        $newfilename = explode('.',$filename)[0] . "." . strtolower($extension);

        if(File::exists(base_path() . '/public/photos/'. $gallery->shareUrl .'/' . $newfilename))
        {
            File::delete(base_path() . '/public/photos/'. $gallery->shareUrl .'/' . $newfilename);                    
        }
        $file->move(
            base_path() . '/public/photos/'. $gallery->shareUrl .'/', $newfilename
        );        
        $photo = Photo::create(['title' => $title, 'url' => $newfilename, 'gallery_id' => $gallery->id]);
        return response()->json(["data" => $photo]);
    }

    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        $user = Auth::user();
        $photo = Photo::where('id', $request->id)->with(['gallery'])->first();
        if(!$photo || $photo->gallery->user_id != $user->id)
            return response()->json(['errors' => ['status' => '400', 'title' => 'A fotó nem törölhető']], 400);
        if(File::delete(base_path() . '/public/photos/'. $photo->gallery->shareUrl .'/' . $photo->url)){
            $photo->delete();
            return response()->json(["data" => ['status' => 'Fotó törölve']]);
        }
        else
            return response()->json(['errors' => ['status' => '400', 'title' => 'A fotó nem törölhető']], 400); 
    }
}
