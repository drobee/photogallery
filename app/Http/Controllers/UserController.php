<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use Illuminate\Support\Facades\Hash as Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Request $request)
    {
        if(!$request->has('name'))
            return response()->json(['errors' => ['status' => '400', 'title' => 'Wrong input']], 400);
        if(!$request->has('password'))
            return response()->json(['errors' => ['status' => '400', 'title' => 'Wrong input']], 400);
        if(!$request->has('email'))
            return response()->json(['errors' => ['status' => '400', 'title' => 'Wrong input']], 400);
        $user = User::create(['name' => $request->name, 'email' => $request->email, 'password' => Hash::make($request->password), 'api_token' => md5($request->name . time())]);
        return response()->json(['data' => $user]);
    }

    public function login(Request $request)
    {
        if(!$request->has('password'))
            return response()->json(['errors' => ['status' => '400', 'title' => 'Wrong input']], 400);
        if(!$request->has('email'))
            return response()->json(['errors' => ['status' => '400', 'title' => 'Wrong input']], 400);
        $user = User::where('email', $request->email)->first();
        if (Hash::check($request->password, $user->password))
            return response()->json(['data' => $user]);
        else
            return response()->json(['errors' => ['status' => '400', 'title' => 'Wrong password']], 400);         
    }

    public function galleries(Request $request)
    {
        $user = Auth::user();
        $galleries = $user->galleries()->with('photos')->get();
        return response()->json(['data' => $galleries]); 
    }
}
