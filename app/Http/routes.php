<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return "Nothing to see here, it's an API...";
});

$app->group(['namespace' => 'App\Http\Controllers'], function () use ($app) {

	$app->get('users', function() {
		return App\User::all();
	});

	$app->post('user/add', 'UserController@create' );

	$app->post('user/login', 'UserController@login' );

	$app->post('user/galleries', 'UserController@galleries' );

	$app->post('gallery/add', ['middleware' => 'auth','uses' => 'GalleryController@create' ]);

	$app->post('gallery/delete', ['middleware' => 'auth','uses' => 'GalleryController@destroy' ]);

	$app->post('gallery/photos', ['middleware' => 'auth','uses' => 'GalleryController@photos' ]);	

	$app->get('gallery/photos/public/{url}', ['uses' => 'GalleryController@publicPhotos' ]);	

	$app->post('photo/add', ['middleware' => 'auth','uses' => 'PhotoController@create' ]);	

	$app->post('photo/delete', ['middleware' => 'auth','uses' => 'PhotoController@destroy' ]);	

});